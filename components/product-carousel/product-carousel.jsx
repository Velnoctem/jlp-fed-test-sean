import styles from "./product-carousel.module.scss";
import { Carousel } from "react-bootstrap";

const ProductCarousel = ({ imageUrls }) => {
  var x = 0;
  return (
    <Carousel variant="dark">
      {imageUrls.map(o => {
        x++
        return <Carousel.Item key={`img_${x}`}>
          <img src={o} alt="" style={{ width: "100%", maxWidth: "500px" }} />
        </Carousel.Item>
      })}
    </Carousel>
  );
};

export default ProductCarousel;

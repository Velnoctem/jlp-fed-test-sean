import styles from "./product-list-item.module.scss";
import Link from "next/link";

const ProductListItem = ({item}) => {
  return (
    <Link
      key={item.productId}
      params={{ id: item.productId }}
      href={{
        pathname: "/product-detail/[id]",
        query: { id: item.productId }
      }}
    >
      <a>
      <div>
        <div className="image">
          <img src={item.image} alt="" style={{ width: "100%" }} />
        </div>
        <div>{item.title}</div>
        <div className={styles.price}>{item.price.now}</div>
      </div>
      </a>
    </Link>
  );
};

export default ProductListItem;

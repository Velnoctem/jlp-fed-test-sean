import Layout from "../components/layout/layout";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import "../styles/globals.scss";
import '../styles/products.scss';

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;

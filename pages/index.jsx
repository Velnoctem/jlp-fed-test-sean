import Head from "next/head";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import data2 from "../mockData/data.json";
import { Container, Row, Col } from "react-bootstrap";

export const GetFirst20 = (data) => {
  return data.slice(0, 20);;
}

export async function getServerSideProps() {
  const d = await GetFirst20(data2.products);
  return {
    props: {
      data: d
    }
  };
}

const Home = ({data}) => {
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <Container className={styles.content} fluid="lg">
        <h3 className="categoryTitle">Dishwashers ({data.length})</h3>
          <Row className={styles.inner}>
              {data.map((item) => (
                <Col data-testid="productItem" key={item.productId} sm={3} xs={6} className="productItem">
                  <ProductListItem item={item} />
                </Col>
              ))}
          </Row>
      </Container>
    </div>
  );
};

export default Home;

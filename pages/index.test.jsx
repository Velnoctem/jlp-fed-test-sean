import React from 'react'
import { render } from "@testing-library/react"
import Home, {GetFirst20} from "./index";
import data from "../mockData/data.json";

it("Renders page", () => {
    render(
        <Home data={data.products} />
    );
})

it("Renders 20 dish washers", () => {
    const {getAllByTestId} = render(
        <Home data={GetFirst20(data.products)} />
    );
    var productCount = getAllByTestId('productItem').length;
    expect(productCount).toBe(20);
})
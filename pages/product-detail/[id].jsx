import ProductCarousel from "../../components/product-carousel/product-carousel";
import data2 from "../../mockData/data2.json";
import { Container, Row, Col } from "react-bootstrap";

export async function getServerSideProps(context) {
  const id = context.params.id;
  const d = await data2.detailsData.find(o => o.productId == id);
  return {
    props: {
      data: d
    }
  };
}

const ProductDetail = ({ data }) => {
  return (
    <Container>
      {data.title &&
        <h3 data-testid="title" className="categoryTitle">{data.title}</h3>
      }
      <Row className="contentProduct">
        <Col xs={12} sm={8}>
          <div className="productLeft">
            <div className="marginBottom15 productCarousel">
              {data.media &&
                <ProductCarousel imageUrls={data.media.images.urls} />
              }
            </div>
            <Col xs={12} className="d-block d-sm-none">
              <div className="productRight">
                {data.price &&
                  <h3 className="black"><strong>£{data.price.now}</strong></h3>
                }
                <div>{data.displaySpecialOffer}</div>
                {data.additionalServices &&
                  <div className="green">{data.additionalServices.includedServices}</div>
                }
              </div>
            </Col>
            {data.productInformation &&
              <div className="productInfo">
                <h3>Product information</h3>
                <div dangerouslySetInnerHTML={{ __html: data.details.productInformation }} />
              </div>
            }
            <div className="readMore">
              <strong>Read More</strong>
            </div>
            {data.details &&
            <div className="productSpec">
              <h3>Product specification</h3>
              <ul>
                {data.details.features[0].attributes.map((item) => (
                  <li key={item.name}>
                    <div><span>{item.name}</span> <span className="float-end">{item.value}</span></div>
                  </li>
                ))}
              </ul>
            </div>
            }
          </div>
        </Col>
        <Col sm={4} className="d-none d-sm-block">
          <div className="productRight">
            {data.price &&
              <h3 className="black"><strong>£{data.price.now}</strong></h3>
            }
            <div>{data.displaySpecialOffer}</div>
            {data.additionalServices &&
              <div className="green">{data.additionalServices.includedServices}</div>
            }
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default ProductDetail;

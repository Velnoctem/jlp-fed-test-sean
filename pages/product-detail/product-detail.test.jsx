import React from 'react'
import { render } from "@testing-library/react"
import ProductDetail from "./[id]";
import data2 from "../../mockData/data2.json";

it("Renders product pages", () => {
    data2.detailsData.forEach(p => {
        const { getAllByTestId } = render(
            <ProductDetail data={p} />
        );
        expect(getAllByTestId("title")).toBeTruthy();
    });
})